extends TileMapWalker

export(NodePath) var PATH_FOLLOW_NODE: NodePath
onready var pathfollow: PathFollow2D = get_node(PATH_FOLLOW_NODE)


func populate_moveset() -> void:
	pathfollow.offset += WORLD.cell_size.x
	var movedir = WORLD.world_to_map(pathfollow.global_position - global_position)
	push_move(movedir)
