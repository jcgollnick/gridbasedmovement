extends TileMapWalker

export(float, 0, 10) var MAX_WAIT_TIME := 2.5
export(float, 0, 1) var REST_CHANCE := 0.2

var rest := Timer.new()
var resting := false


func _init() -> void:
	randomize()
	add_child(rest)
	rest.connect("timeout", self, "_on_RestTimer_timeout")


func populate_moveset() -> void:
	if resting:
		return

	if randf() < REST_CHANCE:
		rest.start(randf() * MAX_WAIT_TIME)
		resting = true
	else:
		push_move((Vector2(randf() - 0.5, randf() - 0.5) * 2.0).round())


func _on_RestTimer_timeout() -> void:
	resting = false
