extends Node2D
class_name TileMapWalker

export(int) var SPEED := 128  # Pixels per second

# These are private fields and should not be accessed by foreign objects.
var _tilepos: Vector2
var _moving := false
var _moves := []
var _move := Vector2.ZERO

export(NodePath) var WORLD_TILEMAP: NodePath
onready var WORLD: TileMap = get_node(WORLD_TILEMAP)

# Populate this array with the tilemap indices that are meant to be non-traversible
# If left empty, all tiles will be solid and only empty cells are traversible
export(Array, int) var WORLD_SOLID_TILES := []


func _ready() -> void:
	assert(WORLD_TILEMAP != "", "World tilemap not set in %s" % get_path())
	assert(is_instance_valid(WORLD), "World tilemap not found in %s" % get_path())

	# Align node to grid
	global_position = global_position.snapped(WORLD.cell_size)
	# Get current tile map position
	_tilepos = WORLD.world_to_map(global_position)


func _process(delta: float) -> void:
	# Moving or not moving is determined by whether on this frame the node is grid-aligned.
	# The only reason to be grid-unaligned is if the node is in transit.
	_moving = not global_position.is_equal_approx(global_position.snapped(WORLD.cell_size))

	if not _moving:
		# Only update the position in the tile map upon each completed grid move.
		_tilepos = WORLD.world_to_map(global_position)

		# At this time determine whether any additional moves should be pushed to the array.
		# All moves pushed to the moveset will be executed synchronously in the order they were pushed (FIFO).
		# This is called at this point so that the next move that needs to happen will initiate on this frame.
		if _moves.size() == 0:
			populate_moveset()

		if _moves.size() > 0:
			_move = _moves.pop_front()
			_moving = true
		else:
			_move = Vector2.ZERO

	if _moving:
		if (
			WORLD_SOLID_TILES.size() == 0
			and WORLD.get_cellv(_tilepos + _move) != TileMap.INVALID_CELL
		):
			return
		elif WORLD.get_cellv(_tilepos + _move) in WORLD_SOLID_TILES:
			return

		var goal_world_pos: Vector2 = WORLD.map_to_world(_tilepos + _move)
		var diff: Vector2 = goal_world_pos - global_position
		var minstep := min(SPEED * delta, max(abs(diff.x), abs(diff.y)))
		global_position = global_position.move_toward(goal_world_pos, minstep)


func populate_moveset() -> void:
	# Override this method to control how and when this node decides to move.
	# This method will only be polled when the moveset is empty.
	# Populate the array based on input to create a controllable player character.
	# Alternatively, push moves based on algorithms to create an AI.
	# Only push moves that match Vector2.UP, DOWN, LEFT or RIGHT with length 1.
	# In order to elide a direct reference to the private member _moves, you can call push_move().
	pass


func push_move(move: Vector2) -> void:
	# This is the preferred interface for pushing a move to the moveset.
	# This method sanitizes the input and will separate compound moves into distinct axis-bound moves.
	# Thus you can call push_move() with a diagonal vector and the moveset will gain one move
	# on the horizontal axis and a separate, subsequent move on the vertical axis.
	if abs(move.x) > 0.0 and abs(move.y) > 0.0:
		_moves.push_back(Vector2(sign(move.x), 0.0))
		_moves.push_back(Vector2(0.0, sign(move.y)))
	else:
		_moves.push_back(Vector2(sign(move.x), sign(move.y)))
